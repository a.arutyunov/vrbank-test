import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';

import axios from 'axios'
Vue.prototype.$axios = axios

Vue.filter('truncate', (str, maxLength = 30) => {
  if (str.length > maxLength) {
    str = str.substring(0, maxLength-3) + '...';
  }
  return str
})

Vue.config.productionTip = false

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
